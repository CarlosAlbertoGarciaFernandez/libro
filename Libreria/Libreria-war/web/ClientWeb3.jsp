<%-- 
    Document   : ClientWeb
    Created on : 4/11/2014, 07:54:26 AM
    Author     : CarlosAlberto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="Entidad.*, stateless.*, java.math.BigDecimal, javax.naming.*, java.util.*"%>
<%!
    private LibroBeanRemote librocat = null;
    String s1, s2, s3;
    Collection list;

    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando Catalogo Bean" + librocat);
        } catch (Exception ex) {
            System.out.println("error " + ex.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s1 = request.getParameter("id");
        if (s1 != null) {
                        
             }
       Libro elemento = librocat.buscalibro(Integer.parseInt(s1));
%>
<p>Registro dado de alta</p>  
<table border="1">
    <tr>
    <td>ID:</td>
    <td>Titulo:</td>
    <td>Autor:</td>
    <td>Precio:</td>
</tr>
<tr>
    <td><%=elemento.getId()%></td>
    <td><%=elemento.getTitulo()%></td>
    <td><%=elemento.getAutor()%></td>
    <td><%=elemento.getPrecio()%></td>
</tr>
<jsp:forward page="forma2.jsp">
    <jsp:param name="id" value="<%=elemento.getId()%>"/>
    <jsp:param name="Titulo" value="<%=elemento.getTitulo()%>"/>
    <jsp:param name="Autor" value="<%=elemento.getAutor()%>"/>
    <jsp:param name="precio" value="<%=elemento.getPrecio()%>"/>
</jsp:forward>
<%
    response.flushBuffer();
%>
</table>
<a href="forma.jsp">Para Regresar</a>      
<%
    } catch (Exception ex) {
        ex.printStackTrace();
    }
%>