<%-- 
    Document   : ClientWeb
    Created on : 4/11/2014, 07:54:26 AM
    Author     : CarlosAlberto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="Entidad.*, stateless.*, java.math.BigDecimal, javax.naming.*, java.util.*"%>
<%!
    private LibroBeanRemote librocat = null;
    String s1, s2, s3, s0;
    Collection list;
    Libro libro;
    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando Catalogo Bean" + librocat);
        } catch (Exception ex) {
            System.out.println("error " + ex.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s0 = request.getParameter("id");
        s1 = request.getParameter("Titulo");
        s2 = request.getParameter("Autor");
        s3 = request.getParameter("precio");
        if (s1 != null && s2 != null && s2 != null && s0 != null) {
            Double precio = new Double(s3);
            BigDecimal b = new BigDecimal(precio);
            int ids=Integer.parseInt(s0);
            librocat.actualizarlibro(ids, s1, s2, b);
            System.out.println("Libro Actualizado");
%>
<p>Libro Actualizado</p>  
<table border="1">
    <tr>
    <td>ID:</td>
    <td>Titulo:</td>
    <td>Autor:</td>
    <td>Precio:</td>
</tr>
<%
    }
    list = librocat.getLibro();
    for (Iterator iter = list.iterator(); iter.hasNext();) {
        Libro elemento = (Libro) iter.next();
%>
<br/>
<tr>
    <td><%=elemento.getId()%></td>
    <td><%=elemento.getTitulo()%></td>
    <td><%=elemento.getAutor()%></td>
    <td><%=elemento.getPrecio()%></td>
</tr>
<%
    }
    response.flushBuffer();
%>
</table>
<a href="buscaid.jsp">Para Regresar</a>      
<%
    } catch (Exception ex) {
        ex.printStackTrace();
    }
%>